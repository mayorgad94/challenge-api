'use strict'

const express = require('express')
const bodyParser =  require('body-parser')
const mongoose = require('mongoose')

const Person = require('./models/person')

const app = express()
const port = process.env.PORT || 3001

app.use(bodyParser.urlencoded({ extended: false}))
app.use(bodyParser.json())

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,Authorization");
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    next();
  });

app.get('/api/person',(req,res) => {
    Person.find({},(err,persons) => {
        if(err) return res.status(500).send({ message: 'Error al realizar la peticion ' })
        if(!persons) return res.status(404).send({ message: 'No existen personas' })

        res.status(200).send({ persons: persons })
    })
})

app.get('/api/person/:personId',(req,res) => {
    let personId= req.params.personId

    Person.findById(personId,(err,person) => {
        if(err) return res.status(500).send({ message: 'Error al realizar la peticion ' })
        if(!person) return res.status(404).send({ message: 'La persona no Existe' })

        res.status(200).send({ person: person })
    })
})

app.post('/api/person',(req,res) => {
    console.log('POST /api/person: ')
    console.log(req.body)

    if(req.body.name != ''){
        let person = new Person()
        person.name =  req.body.name

        person.save((err,personStored) =>{
            if(err) res.status(500).send({ message: `Error al guardar en la Base de datos}: ${err}` })

            res.status(200).send({person : personStored})
        })
    }else{
        res.status(500).send({ message: 'Debes ingresar el campo nombre' })
    }
})

app.put('/api/person/:personId',(req,res) => {
    let personId= req.params.personId
    let update = req.body

    Person.findByIdAndUpdate(personId, update, (err,personUpdated) => {
        if(err) return res.status(500).send({ message: 'Error al actualizar la persona ' })

        res.status(200).send({ person : personUpdated})
    })
})

app.delete('/api/person/:personId',(req,res) => {
    let personId= req.params.personId

    Person.findById(personId,(err,person) => {
        if(err) return res.status(500).send({ message: 'Error al borrar la persona ' })

        person.remove(err => {
            if(err) return res.status(500).send({ message: 'Error al borrar la persona ' })
            res.status(200).send({ message: 'La persona ha sido eliminada' })
        })
    })
})


mongoose.connect('mongodb://diegoMayorga:manu18782@ds027348.mlab.com:27348/heroku_hbp6d5v5', (err,res) =>{
    if(err){
        return console.log(`Error al conectar a la base de datos: ${err}`)
    }
    console.log('Conexión a la base de datos establecida')
    app.listen(port, () => {
        console.log(`API REST corriendo en http://localhost:${port}`);
    })
})